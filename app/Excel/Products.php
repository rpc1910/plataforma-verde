<?php

namespace App\Excel;

use App\Imports\ProductsImport;
use App\Models\Category;
use App\Models\Product;

class Products {

    function import($path) {
        try {
            $file = \Excel::toCollection(new ProductsImport, $path)->first();

            $category = $file->firstWhere('0', 'Category')->all();

            $resultCategory = Category::find($category[1]);

            if(!$resultCategory) {
                $resultCategory = Category::create([
                    'id' => $category[1],
                    'name' => 'Not defined'
                ]);
            }

            $headers = $file->firstWhere('0', 'id')->all();

            foreach($file AS $row) {
                $rowKey = array_combine($headers, $row->all());
                if($this->_validateRow($rowKey)) {
                    $insert = $rowKey;
                    $insert['category_id'] = $resultCategory->id;

                    $resultProduct = Product::find($insert['id']);

                    if($resultProduct) {
                        $resultProduct->update($insert);
                    }
                    else {
                        Product::create($insert);
                    }
                }
            }
        }
        catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function _validateRow($row) {
        $status = true;

        if(!is_numeric($row['id'])) $status = false;
        if(!is_numeric($row['free_shipping'])) $status = false;
        if(!is_numeric($row['price'])) $status = false;

        return $status;
    }
}
