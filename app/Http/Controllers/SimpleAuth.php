<?php

namespace App\Http\Controllers;

use App\Http\Requests\SimpleAuthLoginRequest;
use Illuminate\Support\Str;

class SimpleAuth extends Controller
{
    function login(SimpleAuthLoginRequest $request) {
        if(\Auth::attempt($request->all())) {
            $user = \Auth::user();

            $token = Str::random(60);

            $user->forceFill([
                'api_token' => \Hash::make($token)
            ])->save();

            return response($user);
        }

        return response()->json(['error' => 'Login incorrect'], 401);
    }

    function logout() {
        $user = \Auth::user();

        $user->forceFill([
            'api_token' => null
        ])->save();

        return response()->json(['status' => 'success']);
    }
}
