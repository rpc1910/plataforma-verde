<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileUploadRequest;
use App\Jobs\ImportProducts;
use App\Models\File;
use Illuminate\Http\Request;

class Files extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::with('status')->paginate();
        return response($files, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FileUploadRequest $request)
    {
        $file = $request->file('file');

        $name = uniqid() . '.' . $file->getClientOriginalExtension();

        $uploaded = $file->move('uploads', $name);

        $insert = [
            'path' => $uploaded->getRealPath(),
            'status_id' => 1
        ];

        $created = File::create($insert);

        ImportProducts::dispatch($created);

        return response($created, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = File::with('status')->findOrFail($id);

        return response($file, 200);
    }
}
