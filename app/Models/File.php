<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //
    protected $fillable = [
        'path',
        'status_id'
    ];

    function status() {
        return $this->hasOne('App\Models\Status', 'id', 'status_id');
    }
}
