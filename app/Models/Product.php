<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable = [
        'id',
        'name',
        'free_shipping',
        'description',
        'price',
        'category_id'
    ];

    function category() {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }
}
