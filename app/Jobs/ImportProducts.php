<?php

namespace App\Jobs;

use App\Excel\Products;
use App\Models\File;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(File $file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Products $products)
    {
        try {
            $this->file->status_id = 2;
            $this->file->save();

            $products->import( $this->file->path );

            $this->file->status_id = 3;
            $this->file->save();
        }
        catch(\Exception $e) {

        }
    }
}
