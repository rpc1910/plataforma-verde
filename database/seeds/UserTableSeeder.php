<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'Rodrigo Passos',
            'email' => 'contato@rodrigopassos.com.br',
            'password' => Hash::make('rodrigo'),
        ];

        \App\User::create($data);
    }
}
