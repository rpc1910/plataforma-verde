<?php

use Illuminate\Database\Seeder;

class StatusTableSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            ['id' => 1, 'name' => 'Waiting'],
            ['id' => 2, 'name' => 'Processing'],
            ['id' => 3, 'name' => 'Processed'],
        );

        foreach($data AS $item) {
            \App\Models\Status::create($item);
        }
    }
}
