<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    private $userInformation = [
        'email' => 'contato@rodrigopassos.com.br',
        'password' => 'rodrigo'
    ];

    public function testRequiresEmailAndLogin()
    {
        $this->json('POST', 'api/login')
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.']
                ]
            ]);
    }

    public function testIncorrectLogin() {
        $this->json('POST', 'api/login', ['email' => 'teste@teste.com', 'password' => '12345'])
            ->assertStatus(401)
            ->assertJson([
                'error' => 'Login incorrect'
            ]);
    }

    public function testLogin() {
        $user = $this->user->toArray();
        unset($user['api_token']);

        $this->json('POST', 'api/login', $this->userInformation)
            ->assertStatus(200)
            ->assertJsonFragment($user);
    }

    public function testUserAuth() {
        $token = $this->user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('GET', 'api/user', [], $headers)
            ->assertStatus(200)
            ->assertJson($this->user->toArray());
    }
}
