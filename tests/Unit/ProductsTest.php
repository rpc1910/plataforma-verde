<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        Category::create([
            'name' => 'Teste'
        ]);

        Product::create([
            'name' => 'Produto Teste',
            'free_shipping' => 0,
            'description' => 'Description',
            'price' => 1000.99,
            'category_id' => 1,
        ]);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testVerifyListAllProducts()
    {
        $headers = ['Authorization' => "Bearer {$this->token}"];

        $this->json('GET', 'api/products', [], $headers)
            ->assertStatus(200)
            ->assertJsonFragment([
                'current_page' => 1
            ]);
    }

    function testShowProduct() {
        $headers = ['Authorization' => "Bearer {$this->token}"];

        $this->json('GET', 'api/products/1', [], $headers)
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Produto Teste',
                'free_shipping' => '0',
                'description' => 'Description',
                'price' => '1000.99',
                'category_id' => '1',
            ]);
    }

    function testEditProduct() {
        $headers = ['Authorization' => "Bearer {$this->token}"];

        $this->json('PUT', 'api/products/1', ['name' => 'Produto Alterado'], $headers)
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'Produto Alterado',
                'free_shipping' => '0',
                'description' => 'Description',
                'price' => '1000.99',
                'category_id' => '1',
            ]);
    }

    function testDeleteProduct() {
        $headers = ['Authorization' => "Bearer {$this->token}"];

        $this->json('DELETE', 'api/products/1', [], $headers)
            ->assertStatus(200);

        $this->json('GET', 'api/products/1', [], $headers)
            ->assertStatus(404);
    }
}
