<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api.'], function() {
    Route::post('/login', ['uses' => 'SimpleAuth@login', 'as' => 'login']);

    Route::group(['middleware' => 'auth:api'], function() {
        Route::post('/logout', ['uses' => 'SimpleAuth@logout', 'as' => 'logout']);

        Route::resource('products', 'Products', ['only' => ['index', 'show', 'update', 'destroy']]);
        Route::resource('files', 'Files', ['only' => ['index', 'store', 'show']]);
    });
});
