# Plataforma Verde
Teste técnico para vaga de desenvolvedor PHP
* Rodrigo Passos
* rodrigo.passos25@gmail.com

## Documentação
Esta sessão irá apresentar uma breve documentação sobre a API.
O sistema foi desenvolvido utilizando a versão 5.8 do Laravel.

### Requisitos de servidor

* php ^7.0
* PHP extension php_zip enabled
* PHP extension php_xml enabled
* PHP extension php_gd2 enabled

### Autenticação
Existe apenas um usuário criado para a aplicação. Antes de acessar os recursos da API é necessário resgatar o token do usuário.
Para isso, realize login na aplicação utilizando o endpoint `/api/login` 

Dados para acesso:
* E-mail: contato@rodrigopassos.com.br
* Senha: rodrigo

Foi utilizado um sistema simples de token presente no próprio Laravel para o desenvolvimento desta interface.

As chamadas seguintes da API devem ser passadas no cabeçalho o parâmetro `Authorization: Bearer $token`


### Endpoints
Abaixo é apresentado uma tabela com os endpoints da aplicação

| Método    | URI                    | Nome                 | Descrição                             | 
|-----------|------------------------|----------------------|---------------------------------------|
| POST      | api/login              | api.login            | Realiza a autenticação do usuário     |
| POST      | api/files              | api.files.store      | Realiza o upload do arquivo           |
| GET|HEAD  | api/files              | api.files.index      | Lista todos os arquivos enviados      |
| GET|HEAD  | api/files/{file}       | api.files.show       | Exibe informações do arquivo enviado  |
| GET|HEAD  | api/products           | api.products.index   | Lista todos os produtos cadastrados   |
| GET|HEAD  | api/products/{product} | api.products.show    | Exibe um produto específico pelo ID   |
| PUT|PATCH | api/products/{product} | api.products.update  | Atualiza informações do produto       |
| DELETE    | api/products/{product} | api.products.destroy | Exclui o produto                      |
| GET|HEAD  | api/user               |                      | Retorna informações do usuário logado |
